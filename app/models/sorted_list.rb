class SortedList
  attr_accessor :strategy

  def initialize strategy
    @strategy = strategy
  end
  
  def set_string str_to_sort
    @str_to_sort = str_to_sort
  end

  def sort
    strategy.sort(@str_to_sort)
  end
end