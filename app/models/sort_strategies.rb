class SortStrategies
  def sort(str)
  end
end

class BubbleSort < SortStrategies
  def sort(str)
    return if !str || str.size <= 1
    swapped = true
    while swapped do
      swapped = false
      0.upto(str.size-2) do |i|
        if str[i] > str[i+1]
          str[i], str[i+1] = str[i+1], str[i]
          swapped = true
        end
      end    
    end
  
    "The sorted value using bubble sort: #{str}"
  end
end

class MergeSort < SortStrategies
  def sort(str)
    "The sorted value using merge sort: #{merge_sort(str.split("")).join("")}"
  end
  
  def merge_sort(array)
    if array.length <= 1
      array
    else
      mid = (array.length / 2).floor
      left = merge_sort(array[0..mid-1])
      right = merge_sort(array[mid..array.length])
      merge(left, right)
    end
  end
  
  def merge(left, right)
    if left.empty?
      right
    elsif right.empty?
      left
    elsif left[0] < right[0]
      [left[0]] + merge(left[1..left.length], right)
    else
      [right[0]] + merge(left, right[1..right.length])
    end
  end
end