class SortStringController < ApplicationController
  def index
    str_to_sort = params[:string_to_sort].blank? ? '' : params[:string_to_sort]
    sort_method = !params[:sort_method].blank? && params[:sort_method] == 'bubble' ? BubbleSort : MergeSort
    
    sorted_list = SortedList.new(sort_method.new)
    sorted_list.set_string(str_to_sort)
    result = sorted_list.sort

    flash[:notice] = result
  end
end
