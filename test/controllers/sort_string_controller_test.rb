require 'test_helper'

class SortStringControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get sort_string_index_url
    assert_response :success
  end
  
  test "should return 'abcdef' when using bubble sort method" do
    get sort_string_index_url, params: {:string_to_sort => 'befdac', :sort_method => "bubble"}
    
    assert_select "span", "The sorted value using bubble sort: abcdef"
  end
  
  test "should return 'abcdef' when using merge sort method" do
    get sort_string_index_url, params: {:string_to_sort => 'befdac', :sort_method => "merge"}
    
    assert_select "span", "The sorted value using merge sort: abcdef"
  end
end
